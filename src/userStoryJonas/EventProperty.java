package userStoryJonas;

import event.model.EventOrt;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import org.orm.PersistentException;

import java.time.LocalDateTime;

public class EventProperty {
  private final SimpleStringProperty bezeichnung;
  private final SimpleStringProperty uhrzeit;
  private final SimpleDoubleProperty entfernung;
  private final EventOrt ort;
  private final double distance;
  private final event.model.Event event;


  public EventProperty(event.model.Event e, double lat, double lon) throws PersistentException {
    bezeichnung = new SimpleStringProperty(e.getBezeichnung());
    event = e;
    LocalDateTime zeitpunkt = e.getZeitpunkt().toLocalDateTime();
    int hour = zeitpunkt.getHour();
    int minute = zeitpunkt.getMinute();
    String zeitString = (hour + ":");
    if (minute < 10) {
      zeitString = (zeitString + "0");
    }
    zeitString = zeitString + minute + " Uhr";
    this.uhrzeit = new SimpleStringProperty(zeitString);
    EventOrt[] orte = e.ort.toArray();
    ort = orte[0];
    distance = 1000 * distanceInKm(lat, lon, ort.getLat(), ort.getLon());
    entfernung = new SimpleDoubleProperty(distance);

  }

  public static double distanceInKm(double lat1, double lon1, double lat2, double lon2) {
    int radius = 6371;

    double lat = Math.toRadians(lat2 - lat1);
    double lon = Math.toRadians(lon2 - lon1);

    double a = Math.sin(lat / 2) * Math.sin(lat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(lon / 2) * Math.sin(lon / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double d = radius * c;

    return Math.abs(d);
  }

  public String getBezeichnung() {
    return bezeichnung.get();
  }

  public SimpleStringProperty bezeichnungProperty() {
    return bezeichnung;
  }

  public String getUhrzeit() {
    return uhrzeit.get();
  }

  public SimpleStringProperty uhrzeitProperty() {
    return uhrzeit;
  }

  public double getEntfernung() {
    return entfernung.get();
  }

  public double getDistance() {
    return distance;
  }

  public SimpleDoubleProperty entfernungProperty() {
    return entfernung;
  }

  public double getLat(){
    return ort.getLat();
  }

  public double getLon(){
    return ort.getLon();
  }



}

