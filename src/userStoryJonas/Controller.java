package userStoryJonas;

import com.gluonhq.charm.down.plugins.Position;
import com.gluonhq.maps.MapPoint;
import com.gluonhq.maps.MapView;
import com.gluonhq.maps.demo.PoiLayer;
import event.model.Event;
import event.model.EventDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;

public class Controller implements Initializable {

  @FXML
  private Label feedbackLabel;

  @FXML
  private TextField longitudeField;

  @FXML
  private TextField latitudeField;

  @FXML
  private TextField radiusField;

  @FXML
  private Button showEventsButton;

  @FXML
  private TableView<EventProperty> EventTableView;

  @FXML
  private TableColumn<EventProperty, String> BezeichnungColumn;

  @FXML
  private TableColumn<EventProperty, String> UhrzeitColumn;

  @FXML
  private TableColumn<EventProperty, Double> EntfernungColumn;

  @FXML
  private Button showMapButton;

  private ArrayList<EventProperty> eventsList;

  private double lat;

  private double lon;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    latitudeField.setText("49,147816");
    longitudeField.setText("9,207776");
    radiusField.setText("2000");

    onShowEventsAction(null);

    //EventTableView.setColumnResizePolicy((param -> true));
  }

  public void onShowEventsAction(javafx.event.ActionEvent actionEvent) {
    try {
      //EventDAO.refresh(EventDAO.getEventByORMID("Die Große Buga Sause"));
      event.model.Event[] events = EventDAO.listEventByQuery(null, null);
      NumberFormat nf = NumberFormat.getInstance(Locale.GERMANY);
      double radius = nf.parse(radiusField.getText()).doubleValue();
      lat = nf.parse(latitudeField.getText()).doubleValue();
      lon = nf.parse(longitudeField.getText()).doubleValue();


      ArrayList<Event> a = new ArrayList<Event>(Arrays.asList(events));
      ArrayList<EventProperty> eventPropertyArrayList = new ArrayList<EventProperty>();
      for (Event e : events) {
        if (e.getZeitpunkt().toLocalDateTime().toLocalDate().equals(LocalDate.now())) {
          eventPropertyArrayList.add(new EventProperty(e, lat, lon));
        }
      }

      if (eventPropertyArrayList.size() == 0) {
        showAlert("Heute finden keine Events statt.");
        return;
      }

      Iterator<EventProperty> it = eventPropertyArrayList.iterator();
      while (it.hasNext()) {
        if (it.next().getDistance() > radius) {
          it.remove();
        }
      }

      if (eventPropertyArrayList.size() == 0) {
        showAlert("Keine Events im angegebenen Radius gefunden");
      }

      eventsList = eventPropertyArrayList;

      ObservableList<EventProperty> oEventList = FXCollections.observableArrayList(eventPropertyArrayList);

            /*
            BezeichnungColumn = new TableColumn("Bezeichnung");
            DatumColumn = new TableColumn("Datum");
            EntfernungColumn = new TableColumn("Entfernung");
            */

      BezeichnungColumn.setCellValueFactory(new PropertyValueFactory<EventProperty, String>("bezeichnung"));
      UhrzeitColumn.setCellValueFactory(new PropertyValueFactory<EventProperty, String>("uhrzeit"));
      EntfernungColumn.setCellValueFactory(new PropertyValueFactory<EventProperty, Double>("entfernung"));
      EntfernungColumn.setCellFactory(col ->
          new TableCell<EventProperty, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
              super.updateItem(item, empty);
              if (empty) {
                setText(null);
              } else {
                setText(String.format("%.0f", item.doubleValue()));
              }
            }
          });


      EventTableView.setItems(oEventList);
      //EventTableView.getColumns().addAll(BezeichnungColumn,DatumColumn,EntfernungColumn);
      EventTableView.refresh();

    } catch (org.orm.PersistentException pe) {
      showAlert("Fehlerhafter Datenbankzugriff");
      pe.printStackTrace();
    } catch (ParseException parExp) {
      showAlert("Gültige Zahlen eingeben");
      parExp.printStackTrace();
    }
    if (actionEvent != null) {
      Alert successInfo = new Alert(Alert.AlertType.INFORMATION);
      successInfo.setTitle("Erfolg!");
      successInfo.setHeaderText(null);
      successInfo.setContentText("Datenbankabfrage erfolgreich");
      successInfo.showAndWait();
    }


  }

  public void showAlert(String message) {
    Alert alert = new Alert(Alert.AlertType.WARNING);
    alert.setTitle("Oups");
    alert.setHeaderText(null);
    alert.setContentText(message);
    alert.showAndWait();
  }

  public void onShowMapAction(javafx.event.ActionEvent actionEvent) {
    Stage mapStage = new Stage();
    mapStage.setTitle("Kartenansicht");
    MapView view = new MapView();
    view.setZoom(3);
    Scene scene = new Scene(view, 500, 500);
    mapStage.setScene(scene);


    Position position = new Position(lat, lon);
    MapPoint positionPoint = new MapPoint(position.getLatitude(), position.getLongitude());

    PoiLayer positionLayer = new PoiLayer();
    positionLayer.addPoint(positionPoint, new Circle(5, Color.RED));


    view.addLayer(positionLayer);
    view.setZoom(16);
    view.setCenter(positionPoint.getLatitude(), positionPoint.getLongitude());

    Iterator<EventProperty> it = eventsList.iterator();
    while (it.hasNext()) {
      EventProperty current = it.next();
      positionLayer.addPopupPoint(new MapPoint(current.getLat(), current.getLon()),
          new Circle(5, Color.GREEN),current.getBezeichnung(),current.getUhrzeit());
    }


    mapStage.show();
  }
}
