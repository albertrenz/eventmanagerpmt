package userStoryAlbert;

import java.net.URL;
import java.util.ResourceBundle;

import event.model.Event;
import event.model.EventDAO;
import event.model.EventOrt;
import event.model.Thema;
import event.model.ThemaDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import org.orm.PersistentException;

public class Controller implements Initializable {

  @FXML
  private Accordion accordion;

  @FXML
  private ComboBox<String> comboBox;


  @Override
  public void initialize(URL location, ResourceBundle resources) {
    try {
      event.model.Thema[] themas = ThemaDAO.listThemaByQuery(null, null);
      for (Thema thema : themas) {
        comboBox.getItems().add(thema.toString());
        comboBox.getSelectionModel().selectFirst();
      }
    } catch (PersistentException e) {
      e.printStackTrace();
    }
    fillAccordion();
  }

  /**
   * Methode die aufgerufen wird, wenn die ComboBox verändert wird.
   */
  public void filterChanged() {
    clearAccordion();
    fillAccordion();
  }

  /**
   * Methode die dem Accordion die TitledPanes hinzufügt.
   */
  private void fillAccordion() {

    String selectedItem = comboBox.getValue();
    try {
      event.model.Event[] events = EventDAO.listEventByQuery(null, null);

      for (Event event : events) {
        if (selectedItem.equals(event.getEventThema().toString())) {
          Label datumUeberschrift = new Label("Datum und Uhrzeit:");
          Label ortueberschrift = new Label("Veranstaltungsort:");
          Label platzhalter = new Label();
          Label datum = new Label();
          Label ort = new Label();

          VBox vbox = new VBox();
          vbox.getChildren().add(datumUeberschrift);

          datum.setText(event.getZeitpunkt().toLocalDateTime().toLocalDate().toString() + " " + event.getZeitpunkt().toLocalDateTime().toLocalTime().toString());
          vbox.getChildren().add(datum);

          vbox.getChildren().add(platzhalter);

          vbox.getChildren().add(ortueberschrift);
          EventOrt[] orte = event.ort.toArray();
          ort.setText(orte[0].toString());
          vbox.getChildren().add(ort);

          TitledPane pane = new TitledPane(event.getBezeichnung(), vbox);
          accordion.getPanes().add(pane);
          accordion.setExpandedPane(pane);
        }
      }


      if (accordion.getPanes().isEmpty()) {
        TitledPane pane = new TitledPane("Zu diesem Filter gibt es noch keine Events.", null);
        accordion.getPanes().add(pane);
        accordion.setExpandedPane(pane);
        Alert incorrect = new Alert(Alert.AlertType.INFORMATION);
        incorrect.setTitle("Eventinformation");
        incorrect.setHeaderText("Zu diesem Filter gibt es noch keine Events.");
        incorrect.setContentText("Sie können aber einen anderen Filter wählen.");
        incorrect.showAndWait();
      }
    } catch (PersistentException e) {
      e.printStackTrace();
    }
  }

  /**
   * Methode welche die TitledPanes aus dem Accordion wieder entfernt.
   */
  private void clearAccordion() {
    accordion.getPanes().clear();
  }
}