package userStoryChristian;

import java.net.URL;
import java.sql.Timestamp;
import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import event.model.*;


import org.orm.PersistentException;


public class EventErstellenController implements Initializable {

  @FXML
  private TabPane accordion;

  @FXML
  private ListView<event.model.Event> listEvents;

  @FXML
  private Button btnEdit;

  @FXML
  private Button btnDel;


  @FXML
  private Tab inputTab;

  @FXML
  private VBox inputField;

  @FXML
  private TextField txtBez;

  @FXML
  private DatePicker txtDate;

  @FXML
  private TextField txtHour;

  @FXML
  private TextField txtMin;

  @FXML
  private ChoiceBox thema;

  @FXML
  private ChoiceBox veranstalter;

  @FXML
  private ChoiceBox ort;

  @FXML
  private Button btnNew2;


  private boolean correctAll;
  private boolean correctHour;
  private boolean correctMin;

  private boolean edit;

  private event.model.Event sel;
  private event.model.Event edited;


  public void initialize(URL location, ResourceBundle rb) {

    try {

      Thema[] ts = ThemaDAO.listThemaByQuery(null, "Bez");
      ObservableList<Thema> ts2 = FXCollections.observableArrayList(ts);
      thema.setItems(ts2);

      ObservableList<event.model.Event> ol = FXCollections.observableArrayList();
      ol.setAll(EventDAO.listEventByQuery(null, "Bezeichnung"));
      listEvents.setItems(ol);
      listEvents.getSelectionModel().selectFirst();

      Veranstalter[] vs = VeranstalterDAO.listVeranstalterByQuery(null, "Username");
      ObservableList<Veranstalter> vs2 = FXCollections.observableArrayList(vs);
      veranstalter.setItems(vs2);

      EventOrt[] os = EventOrtDAO.listEventOrtByQuery(null, "Name");
      ObservableList<EventOrt> os2 = FXCollections.observableArrayList(os);
      ort.setItems(os2);

    } catch (PersistentException e) {
    }
  }

  @FXML
  void focusLoss(MouseEvent event) {

    if (!listEvents.focusedProperty().getValue()) {
      btnDel.setDisable(true);
      btnEdit.setDisable(true);
    }
  }

  @FXML
  void selectEvent(MouseEvent event) {

    sel = listEvents.getFocusModel().getFocusedItem();
    btnDel.setDisable(false);
    btnEdit.setDisable(false);
  }

  @FXML
  void newEvent(ActionEvent event) {

    btnDel.setDisable(true);
    btnEdit.setDisable(true);
    btnNew2.setVisible(false);
    inputTab.setDisable(false);
    inputField.setVisible(true);

    txtBez.editableProperty().setValue(true);
    edit = false;

    txtBez.setText("");
    Clock now = Clock.systemDefaultZone();
    LocalDate ld = LocalDate.now(now);
    txtDate.setValue(ld);
    txtHour.setText("");
    txtMin.setText("");
    thema.getSelectionModel().selectFirst();
    veranstalter.getSelectionModel().selectFirst();
    ort.getSelectionModel().selectFirst();

    accordion.getSelectionModel().select(1);
    Tab t2 = accordion.getTabs().get(1);
    t2.setText("    Erstellen    ");
  }

  @FXML
  void delEvent(ActionEvent event) {

    try {
      btnDel.setDisable(true);
      btnEdit.setDisable(true);

      EventDAO.deleteAndDissociate(sel);

      ObservableList<event.model.Event> ol = FXCollections.observableArrayList();
      ol.setAll(EventDAO.listEventByQuery(null, "Bezeichnung"));
      listEvents.setItems(ol);
      listEvents.getSelectionModel().selectFirst();

      Alert deleted = new Alert(Alert.AlertType.INFORMATION);
      deleted.setTitle("Erfolg");
      deleted.setHeaderText("Event erfolgreich abgesagt.");
      deleted.showAndWait();

    } catch (PersistentException e) {
    }
  }

  @FXML
  void editEvent(ActionEvent event) {

    btnDel.setDisable(true);
    btnEdit.setDisable(true);
    btnNew2.setVisible(true);
    inputTab.setDisable(false);
    inputField.setVisible(true);

    txtBez.editableProperty().setValue(false);
    edit = true;
    edited = sel;

    txtBez.setText(edited.getBezeichnung());
    LocalDate ld = edited.getZeitpunkt().toLocalDateTime().toLocalDate();
    txtDate.setValue(ld);
    Timestamp time = edited.getZeitpunkt();
    txtHour.setText(Integer.toString(time.getHours()));
    txtMin.setText(Integer.toString(time.getMinutes()));
    thema.setValue(edited.getEventThema());
    veranstalter.setValue(edited.getVerantwortlicher());
    ort.setValue(edited.ort.toArray()[0]);

    accordion.getSelectionModel().select(1);
    Tab t2 = accordion.getTabs().get(1);
    t2.setText("    Bearbeiten    ");
  }

  @FXML
  void confirm(ActionEvent event) {

    correctAll = true;
    correctHour = true;
    correctMin = true;

    int st = 0;
    int min = 0;

    try {
      st = Integer.parseInt(txtHour.getText());
      if (st == -0) {
        txtHour.setText("0");
        st = 0;
      }
      if (st > 23 || st < 0) {
        correctHour = false;
      }
    } catch (NumberFormatException e) {
      correctHour = false;
    }

    if (correctHour) {
      txtHour.setStyle("-fx-border-style: none");
    } else {
      txtHour.setStyle("-fx-border-color: #FF0000;-fx-border-radius: 3;");
      correctAll = false;
    }

    try {
      min = Integer.parseInt(txtMin.getText());
      if (min == -0) {
        txtMin.setText("0");
        min = 0;
      }
      if (min > 59 || min < 0) {
        correctMin = false;
      }
    } catch (NumberFormatException e) {
      correctMin = false;
    }

    if (correctMin) {
      txtMin.setStyle("-fx-border-style: none");
    } else {
      txtMin.setStyle("-fx-border-color: #FF0000;-fx-border-radius: 3;");
      correctAll = false;
    }

    if (txtBez.getText().equals("")) {
      txtBez.setStyle("-fx-border-color: #FF0000;-fx-border-radius: 3;");
      correctAll = false;
    } else {
      txtBez.setStyle("-fx-border-style: none");
    }

    if (correctAll) {

      event.model.Event neu;
      try {

        if (edit == true) {
          neu = EventDAO.loadEventByORMID(edited.getBezeichnung());
        } else {
          neu = EventDAO.createEvent();
        }

        neu.setBezeichnung(txtBez.getText());
        LocalDate dat = txtDate.getValue();
        Timestamp time = Timestamp.valueOf(dat.atStartOfDay());
        time.setHours(st);
        time.setMinutes(min);
        neu.setZeitpunkt(time);
        neu.setEventThema((Thema) thema.getValue());
        neu.setVerantwortlicher((Veranstalter) veranstalter.getValue());
        neu.ort.add((EventOrt) ort.getValue());

        EventDAO.save(neu);

        ObservableList<event.model.Event> ol = FXCollections.observableArrayList();
        ol.setAll(EventDAO.listEventByQuery(null, "Bezeichnung"));
        listEvents.setItems(ol);

        Alert success = new Alert(Alert.AlertType.INFORMATION);
        success.setTitle("Erfolg");
        success.setHeaderText("Eventdaten erfolgreich übertragen.");
        success.showAndWait();

      } catch (PersistentException e) {
          Alert notUnique = new Alert(Alert.AlertType.ERROR);
          notUnique.setTitle("Fehler");
          notUnique.setHeaderText("Ein Event mit diesem Namen findet bereits statt.");
          notUnique.showAndWait();
      }
    } else {
      Alert incorrect = new Alert(Alert.AlertType.ERROR);
      incorrect.setTitle("Fehler");
      incorrect.setHeaderText("Ungültige Eingaben abändern:");
      String incorrectFields = new String("");
      if (txtBez.getText().equals("")) {
        incorrectFields += "- Bezeichnung" + "\n";
      }
      if (!correctHour) {
        incorrectFields += "- Stunde" + "\n";
      }
      if (!correctMin) {
        incorrectFields += "- Minute" + "\n";
      }
      incorrect.setContentText(incorrectFields);
      incorrect.showAndWait();
    }

  }


}