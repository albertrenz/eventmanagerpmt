/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Buehne")
public class Buehne implements Serializable {
	public Buehne() {
	}
	
	public boolean equals(Object aObj) {
		if (aObj == this)
			return true;
		if (!(aObj instanceof Buehne))
			return false;
		Buehne buehne = (Buehne)aObj;
		if ((getBez() != null && !getBez().equals(buehne.getBez())) || (getBez() == null && buehne.getBez() != null))
			return false;
		return true;
	}
	
	public int hashCode() {
		int hashcode = 0;
		hashcode = hashcode + (getBez() == null ? 0 : getBez().hashCode());
		return hashcode;
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_BUEHNE_ORT) {
			return ORM_ort;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="Bez", nullable=false, length=255)	
	@Id	
	private String bez;
	
	@OneToMany(mappedBy="BuehnenArt", targetEntity=event.model.EventOrt.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_ort = new java.util.HashSet();
	
	public void setBez(String value) {
		this.bez = value;
	}
	
	public String getBez() {
		return bez;
	}
	
	public String getORMID() {
		return getBez();
	}
	
	private void setORM_Ort(java.util.Set value) {
		this.ORM_ort = value;
	}
	
	private java.util.Set getORM_Ort() {
		return ORM_ort;
	}
	
	@Transient	
	public final event.model.EventOrtSetCollection ort = new event.model.EventOrtSetCollection(this, _ormAdapter, ORMConstants.KEY_BUEHNE_ORT, ORMConstants.KEY_EVENTORT_BUEHNENART, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getBez());
	}
	
}
