/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_ADMINISTRATOR_VERANTWORTLICHER = 1040017471;
	
	final int KEY_BESUCHER_EVENTS = -1584916703;
	
	final int KEY_BUEHNE_ORT = -117085153;
	
	final int KEY_DARSTELLER_AUFTRITTE = -906959319;
	
	final int KEY_EVENTORT_BUEHNENART = 561778762;
	
	final int KEY_EVENTORT_EVENTS = -854677407;
	
	final int KEY_EVENT_ARTISTEN = 1171681909;
	
	final int KEY_EVENT_EVENTTHEMA = 1048306256;
	
	final int KEY_EVENT_ORT = 2035686284;
	
	final int KEY_EVENT_TEILNEHMER = -492242430;
	
	final int KEY_EVENT_VERANTWORTLICHER = -1722102766;
	
	final int KEY_THEMA_EVENTS = -8482541;
	
	final int KEY_VERANSTALTER_ADMIN = -492279851;
	
	final int KEY_VERANSTALTER_EVENTS = 2050100563;
	
}
