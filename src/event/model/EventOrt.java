/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import java.io.Serializable;
import javax.persistence.*;
/**
 * -name
 * -lat : float
 * -lon : float
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="EventOrt")
public class EventOrt implements Serializable {
	public EventOrt() {
	}
	
	public boolean equals(Object aObj) {
		if (aObj == this)
			return true;
		if (!(aObj instanceof EventOrt))
			return false;
		EventOrt eventort = (EventOrt)aObj;
		if ((getName() != null && !getName().equals(eventort.getName())) || (getName() == null && eventort.getName() != null))
			return false;
		return true;
	}
	
	public int hashCode() {
		int hashcode = 0;
		hashcode = hashcode + (getName() == null ? 0 : getName().hashCode());
		return hashcode;
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_EVENTORT_EVENTS) {
			return ORM_events;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_EVENTORT_BUEHNENART) {
			this.BuehnenArt = (event.model.Buehne) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="Name", nullable=false, length=255)	
	@Id	
	private String name;
	
	@Column(name="Lat", nullable=false)	
	private float lat;
	
	@Column(name="Lon", nullable=false)	
	private float lon;
	
	@Column(name="Kapazität", nullable=false, length=10)	
	private int kapazität;
	
	@ManyToOne(targetEntity=event.model.Buehne.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="BuehneBez", referencedColumnName="Bez", nullable=false) })	
	private event.model.Buehne BuehnenArt;
	
	@ManyToMany(targetEntity=event.model.Event.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinTable(name="Event_EventOrt", joinColumns={ @JoinColumn(name="EventOrtName") }, inverseJoinColumns={ @JoinColumn(name="EventBezeichnung") })	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_events = new java.util.HashSet();
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public String getORMID() {
		return getName();
	}
	
	public void setLat(float value) {
		this.lat = value;
	}
	
	public float getLat() {
		return lat;
	}
	
	public void setLon(float value) {
		this.lon = value;
	}
	
	public float getLon() {
		return lon;
	}
	
	public void setKapazität(int value) {
		this.kapazität = value;
	}
	
	public int getKapazität() {
		return kapazität;
	}
	
	private void setORM_Events(java.util.Set value) {
		this.ORM_events = value;
	}
	
	private java.util.Set getORM_Events() {
		return ORM_events;
	}
	
	@Transient	
	public final event.model.EventSetCollection events = new event.model.EventSetCollection(this, _ormAdapter, ORMConstants.KEY_EVENTORT_EVENTS, ORMConstants.KEY_EVENT_ORT, ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public void setBuehnenArt(event.model.Buehne value) {
		if (BuehnenArt != null) {
			BuehnenArt.ort.remove(this);
		}
		if (value != null) {
			value.ort.add(this);
		}
	}
	
	public event.model.Buehne getBuehnenArt() {
		return BuehnenArt;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_BuehnenArt(event.model.Buehne value) {
		this.BuehnenArt = value;
	}
	
	private event.model.Buehne getORM_BuehnenArt() {
		return BuehnenArt;
	}
	
	public String toString() {
		return String.valueOf(getName());
	}
	
}
