/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import java.io.Serializable;
import javax.persistence.*;
@MappedSuperclass
public class Benutzer implements Serializable {
	public Benutzer() {
	}
	
	@Column(name="ID", nullable=false, length=3)	
	@Id	
	@GeneratedValue(generator="EVENT_MODEL_BENUTZER_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="EVENT_MODEL_BENUTZER_ID_GENERATOR", strategy="native")	
	protected int ID;
	
	@Column(name="Vorname", nullable=true, length=255)	
	protected String vorname;
	
	@Column(name="Nachname", nullable=true, length=255)	
	protected String nachname;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setVorname(String value) {
		this.vorname = value;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setNachname(String value) {
		this.nachname = value;
	}
	
	public String getNachname() {
		return nachname;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
