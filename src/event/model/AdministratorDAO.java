/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class AdministratorDAO {
	public static Administrator loadAdministratorByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadAdministratorByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator getAdministratorByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getAdministratorByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator loadAdministratorByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadAdministratorByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator getAdministratorByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getAdministratorByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator loadAdministratorByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Administrator) session.load(event.model.Administrator.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator getAdministratorByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Administrator) session.get(event.model.Administrator.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator loadAdministratorByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Administrator) session.load(event.model.Administrator.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator getAdministratorByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Administrator) session.get(event.model.Administrator.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryAdministrator(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryAdministrator(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryAdministrator(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryAdministrator(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator[] listAdministratorByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listAdministratorByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator[] listAdministratorByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listAdministratorByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryAdministrator(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Administrator as Administrator");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryAdministrator(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Administrator as Administrator");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Administrator", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator[] listAdministratorByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryAdministrator(session, condition, orderBy);
			return (Administrator[]) list.toArray(new Administrator[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator[] listAdministratorByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryAdministrator(session, condition, orderBy, lockMode);
			return (Administrator[]) list.toArray(new Administrator[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator loadAdministratorByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadAdministratorByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator loadAdministratorByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadAdministratorByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator loadAdministratorByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Administrator[] administrators = listAdministratorByQuery(session, condition, orderBy);
		if (administrators != null && administrators.length > 0)
			return administrators[0];
		else
			return null;
	}
	
	public static Administrator loadAdministratorByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Administrator[] administrators = listAdministratorByQuery(session, condition, orderBy, lockMode);
		if (administrators != null && administrators.length > 0)
			return administrators[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateAdministratorByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateAdministratorByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAdministratorByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateAdministratorByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAdministratorByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Administrator as Administrator");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateAdministratorByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Administrator as Administrator");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Administrator", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Administrator createAdministrator() {
		return new event.model.Administrator();
	}
	
	public static boolean save(event.model.Administrator administrator) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().saveObject(administrator);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(event.model.Administrator administrator) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().deleteObject(administrator);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Administrator administrator)throws PersistentException {
		try {
			event.model.Veranstalter[] lVerantwortlichers = administrator.verantwortlicher.toArray();
			for(int i = 0; i < lVerantwortlichers.length; i++) {
				lVerantwortlichers[i].setAdmin(null);
			}
			return delete(administrator);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Administrator administrator, org.orm.PersistentSession session)throws PersistentException {
		try {
			event.model.Veranstalter[] lVerantwortlichers = administrator.verantwortlicher.toArray();
			for(int i = 0; i < lVerantwortlichers.length; i++) {
				lVerantwortlichers[i].setAdmin(null);
			}
			try {
				session.delete(administrator);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(event.model.Administrator administrator) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().refresh(administrator);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(event.model.Administrator administrator) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().evict(administrator);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
