/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Darsteller")
public class Darsteller implements Serializable {
	public Darsteller() {
	}
	
	public boolean equals(Object aObj) {
		if (aObj == this)
			return true;
		if (!(aObj instanceof Darsteller))
			return false;
		Darsteller darsteller = (Darsteller)aObj;
		if ((getKünstlerName() != null && !getKünstlerName().equals(darsteller.getKünstlerName())) || (getKünstlerName() == null && darsteller.getKünstlerName() != null))
			return false;
		return true;
	}
	
	public int hashCode() {
		int hashcode = 0;
		hashcode = hashcode + (getKünstlerName() == null ? 0 : getKünstlerName().hashCode());
		return hashcode;
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_DARSTELLER_AUFTRITTE) {
			return ORM_auftritte;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="KünstlerName", nullable=false, length=255)	
	@Id	
	private String künstlerName;
	
	@ManyToMany(mappedBy="ORM_artisten", targetEntity=event.model.Event.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_auftritte = new java.util.HashSet();
	
	public void setKünstlerName(String value) {
		this.künstlerName = value;
	}
	
	public String getKünstlerName() {
		return künstlerName;
	}
	
	public String getORMID() {
		return getKünstlerName();
	}
	
	private void setORM_Auftritte(java.util.Set value) {
		this.ORM_auftritte = value;
	}
	
	private java.util.Set getORM_Auftritte() {
		return ORM_auftritte;
	}
	
	@Transient	
	public final event.model.EventSetCollection auftritte = new event.model.EventSetCollection(this, _ormAdapter, ORMConstants.KEY_DARSTELLER_AUFTRITTE, ORMConstants.KEY_EVENT_ARTISTEN, ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public String toString() {
		return String.valueOf(getKünstlerName());
	}
	
}
