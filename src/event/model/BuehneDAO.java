/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class BuehneDAO {
	public static Buehne loadBuehneByORMID(String bez) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBuehneByORMID(session, bez);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne getBuehneByORMID(String bez) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getBuehneByORMID(session, bez);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne loadBuehneByORMID(String bez, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBuehneByORMID(session, bez, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne getBuehneByORMID(String bez, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getBuehneByORMID(session, bez, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne loadBuehneByORMID(PersistentSession session, String bez) throws PersistentException {
		try {
			return (Buehne) session.load(event.model.Buehne.class, bez);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne getBuehneByORMID(PersistentSession session, String bez) throws PersistentException {
		try {
			return (Buehne) session.get(event.model.Buehne.class, bez);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne loadBuehneByORMID(PersistentSession session, String bez, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Buehne) session.load(event.model.Buehne.class, bez, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne getBuehneByORMID(PersistentSession session, String bez, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Buehne) session.get(event.model.Buehne.class, bez, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBuehne(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryBuehne(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBuehne(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryBuehne(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne[] listBuehneByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listBuehneByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne[] listBuehneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listBuehneByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBuehne(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Buehne as Buehne");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBuehne(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Buehne as Buehne");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Buehne", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne[] listBuehneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryBuehne(session, condition, orderBy);
			return (Buehne[]) list.toArray(new Buehne[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne[] listBuehneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryBuehne(session, condition, orderBy, lockMode);
			return (Buehne[]) list.toArray(new Buehne[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne loadBuehneByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBuehneByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne loadBuehneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBuehneByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne loadBuehneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Buehne[] buehnes = listBuehneByQuery(session, condition, orderBy);
		if (buehnes != null && buehnes.length > 0)
			return buehnes[0];
		else
			return null;
	}
	
	public static Buehne loadBuehneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Buehne[] buehnes = listBuehneByQuery(session, condition, orderBy, lockMode);
		if (buehnes != null && buehnes.length > 0)
			return buehnes[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateBuehneByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateBuehneByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBuehneByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateBuehneByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBuehneByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Buehne as Buehne");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBuehneByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Buehne as Buehne");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Buehne", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Buehne createBuehne() {
		return new event.model.Buehne();
	}
	
	public static boolean save(event.model.Buehne buehne) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().saveObject(buehne);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(event.model.Buehne buehne) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().deleteObject(buehne);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Buehne buehne)throws PersistentException {
		try {
			event.model.EventOrt[] lOrts = buehne.ort.toArray();
			for(int i = 0; i < lOrts.length; i++) {
				lOrts[i].setBuehnenArt(null);
			}
			return delete(buehne);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Buehne buehne, org.orm.PersistentSession session)throws PersistentException {
		try {
			event.model.EventOrt[] lOrts = buehne.ort.toArray();
			for(int i = 0; i < lOrts.length; i++) {
				lOrts[i].setBuehnenArt(null);
			}
			try {
				session.delete(buehne);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(event.model.Buehne buehne) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().refresh(buehne);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(event.model.Buehne buehne) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().evict(buehne);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
