/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Veranstalter")
public class Veranstalter extends event.model.Benutzer implements Serializable {
	public Veranstalter() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_VERANSTALTER_EVENTS) {
			return ORM_events;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_VERANSTALTER_ADMIN) {
			this.Admin = (event.model.Administrator) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="Username", nullable=false, length=255)	
	private String username;
	
	@Column(name="Passwort", nullable=true, length=255)	
	private String passwort;
	
	@ManyToOne(targetEntity=event.model.Administrator.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="AdministratorID", referencedColumnName="ID", nullable=false) })	
	private event.model.Administrator Admin;
	
	@OneToMany(mappedBy="Verantwortlicher", targetEntity=event.model.Event.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_events = new java.util.HashSet();
	
	public void setUsername(String value) {
		this.username = value;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPasswort(String value) {
		this.passwort = value;
	}
	
	public String getPasswort() {
		return passwort;
	}
	
	public void setAdmin(event.model.Administrator value) {
		if (Admin != null) {
			Admin.verantwortlicher.remove(this);
		}
		if (value != null) {
			value.verantwortlicher.add(this);
		}
	}
	
	public event.model.Administrator getAdmin() {
		return Admin;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Admin(event.model.Administrator value) {
		this.Admin = value;
	}
	
	private event.model.Administrator getORM_Admin() {
		return Admin;
	}
	
	private void setORM_Events(java.util.Set value) {
		this.ORM_events = value;
	}
	
	private java.util.Set getORM_Events() {
		return ORM_events;
	}
	
	@Transient	
	public final event.model.EventSetCollection events = new event.model.EventSetCollection(this, _ormAdapter, ORMConstants.KEY_VERANSTALTER_EVENTS, ORMConstants.KEY_EVENT_VERANTWORTLICHER, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return super.toString();
	}
	
}
