/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DarstellerDAO {
	public static Darsteller loadDarstellerByORMID(String künstlerName) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadDarstellerByORMID(session, künstlerName);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller getDarstellerByORMID(String künstlerName) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getDarstellerByORMID(session, künstlerName);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller loadDarstellerByORMID(String künstlerName, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadDarstellerByORMID(session, künstlerName, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller getDarstellerByORMID(String künstlerName, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getDarstellerByORMID(session, künstlerName, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller loadDarstellerByORMID(PersistentSession session, String künstlerName) throws PersistentException {
		try {
			return (Darsteller) session.load(event.model.Darsteller.class, künstlerName);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller getDarstellerByORMID(PersistentSession session, String künstlerName) throws PersistentException {
		try {
			return (Darsteller) session.get(event.model.Darsteller.class, künstlerName);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller loadDarstellerByORMID(PersistentSession session, String künstlerName, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Darsteller) session.load(event.model.Darsteller.class, künstlerName, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller getDarstellerByORMID(PersistentSession session, String künstlerName, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Darsteller) session.get(event.model.Darsteller.class, künstlerName, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDarsteller(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryDarsteller(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDarsteller(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryDarsteller(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller[] listDarstellerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listDarstellerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller[] listDarstellerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listDarstellerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDarsteller(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Darsteller as Darsteller");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDarsteller(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Darsteller as Darsteller");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Darsteller", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller[] listDarstellerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryDarsteller(session, condition, orderBy);
			return (Darsteller[]) list.toArray(new Darsteller[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller[] listDarstellerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryDarsteller(session, condition, orderBy, lockMode);
			return (Darsteller[]) list.toArray(new Darsteller[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller loadDarstellerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadDarstellerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller loadDarstellerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadDarstellerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller loadDarstellerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Darsteller[] darstellers = listDarstellerByQuery(session, condition, orderBy);
		if (darstellers != null && darstellers.length > 0)
			return darstellers[0];
		else
			return null;
	}
	
	public static Darsteller loadDarstellerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Darsteller[] darstellers = listDarstellerByQuery(session, condition, orderBy, lockMode);
		if (darstellers != null && darstellers.length > 0)
			return darstellers[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateDarstellerByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateDarstellerByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDarstellerByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateDarstellerByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDarstellerByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Darsteller as Darsteller");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDarstellerByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Darsteller as Darsteller");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Darsteller", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Darsteller createDarsteller() {
		return new event.model.Darsteller();
	}
	
	public static boolean save(event.model.Darsteller darsteller) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().saveObject(darsteller);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(event.model.Darsteller darsteller) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().deleteObject(darsteller);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Darsteller darsteller)throws PersistentException {
		try {
			event.model.Event[] lAuftrittes = darsteller.auftritte.toArray();
			for(int i = 0; i < lAuftrittes.length; i++) {
				lAuftrittes[i].artisten.remove(darsteller);
			}
			return delete(darsteller);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Darsteller darsteller, org.orm.PersistentSession session)throws PersistentException {
		try {
			event.model.Event[] lAuftrittes = darsteller.auftritte.toArray();
			for(int i = 0; i < lAuftrittes.length; i++) {
				lAuftrittes[i].artisten.remove(darsteller);
			}
			try {
				session.delete(darsteller);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(event.model.Darsteller darsteller) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().refresh(darsteller);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(event.model.Darsteller darsteller) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().evict(darsteller);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
