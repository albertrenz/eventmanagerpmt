/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Administrator")
public class Administrator extends event.model.Benutzer implements Serializable {
	public Administrator() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_ADMINISTRATOR_VERANTWORTLICHER) {
			return ORM_verantwortlicher;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="Username", nullable=false, length=255)	
	private String username;
	
	@Column(name="Passwort", nullable=true, length=255)	
	private String passwort;
	
	@OneToMany(mappedBy="Admin", targetEntity=event.model.Veranstalter.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_verantwortlicher = new java.util.HashSet();
	
	public void setUsername(String value) {
		this.username = value;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPasswort(String value) {
		this.passwort = value;
	}
	
	public String getPasswort() {
		return passwort;
	}
	
	private void setORM_Verantwortlicher(java.util.Set value) {
		this.ORM_verantwortlicher = value;
	}
	
	private java.util.Set getORM_Verantwortlicher() {
		return ORM_verantwortlicher;
	}
	
	@Transient	
	public final event.model.VeranstalterSetCollection verantwortlicher = new event.model.VeranstalterSetCollection(this, _ormAdapter, ORMConstants.KEY_ADMINISTRATOR_VERANTWORTLICHER, ORMConstants.KEY_VERANSTALTER_ADMIN, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return super.toString();
	}
	
}
