/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class BesucherDAO {
	public static Besucher loadBesucherByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBesucherByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher getBesucherByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getBesucherByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher loadBesucherByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBesucherByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher getBesucherByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getBesucherByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher loadBesucherByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Besucher) session.load(event.model.Besucher.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher getBesucherByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Besucher) session.get(event.model.Besucher.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher loadBesucherByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Besucher) session.load(event.model.Besucher.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher getBesucherByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Besucher) session.get(event.model.Besucher.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBesucher(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryBesucher(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBesucher(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryBesucher(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher[] listBesucherByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listBesucherByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher[] listBesucherByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listBesucherByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBesucher(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Besucher as Besucher");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBesucher(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Besucher as Besucher");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Besucher", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher[] listBesucherByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryBesucher(session, condition, orderBy);
			return (Besucher[]) list.toArray(new Besucher[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher[] listBesucherByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryBesucher(session, condition, orderBy, lockMode);
			return (Besucher[]) list.toArray(new Besucher[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher loadBesucherByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBesucherByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher loadBesucherByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadBesucherByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher loadBesucherByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Besucher[] besuchers = listBesucherByQuery(session, condition, orderBy);
		if (besuchers != null && besuchers.length > 0)
			return besuchers[0];
		else
			return null;
	}
	
	public static Besucher loadBesucherByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Besucher[] besuchers = listBesucherByQuery(session, condition, orderBy, lockMode);
		if (besuchers != null && besuchers.length > 0)
			return besuchers[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateBesucherByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateBesucherByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBesucherByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateBesucherByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBesucherByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Besucher as Besucher");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBesucherByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Besucher as Besucher");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Besucher", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Besucher createBesucher() {
		return new event.model.Besucher();
	}
	
	public static boolean save(event.model.Besucher besucher) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().saveObject(besucher);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(event.model.Besucher besucher) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().deleteObject(besucher);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Besucher besucher)throws PersistentException {
		try {
			event.model.Event[] lEventss = besucher.events.toArray();
			for(int i = 0; i < lEventss.length; i++) {
				lEventss[i].teilnehmer.remove(besucher);
			}
			return delete(besucher);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Besucher besucher, org.orm.PersistentSession session)throws PersistentException {
		try {
			event.model.Event[] lEventss = besucher.events.toArray();
			for(int i = 0; i < lEventss.length; i++) {
				lEventss[i].teilnehmer.remove(besucher);
			}
			try {
				session.delete(besucher);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(event.model.Besucher besucher) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().refresh(besucher);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(event.model.Besucher besucher) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().evict(besucher);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
