/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class VeranstalterDAO {
	public static Veranstalter loadVeranstalterByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadVeranstalterByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter getVeranstalterByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getVeranstalterByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter loadVeranstalterByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadVeranstalterByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter getVeranstalterByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return getVeranstalterByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter loadVeranstalterByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Veranstalter) session.load(event.model.Veranstalter.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter getVeranstalterByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Veranstalter) session.get(event.model.Veranstalter.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter loadVeranstalterByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Veranstalter) session.load(event.model.Veranstalter.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter getVeranstalterByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Veranstalter) session.get(event.model.Veranstalter.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVeranstalter(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryVeranstalter(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVeranstalter(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return queryVeranstalter(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter[] listVeranstalterByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listVeranstalterByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter[] listVeranstalterByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return listVeranstalterByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVeranstalter(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Veranstalter as Veranstalter");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVeranstalter(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Veranstalter as Veranstalter");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Veranstalter", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter[] listVeranstalterByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryVeranstalter(session, condition, orderBy);
			return (Veranstalter[]) list.toArray(new Veranstalter[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter[] listVeranstalterByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryVeranstalter(session, condition, orderBy, lockMode);
			return (Veranstalter[]) list.toArray(new Veranstalter[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter loadVeranstalterByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadVeranstalterByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter loadVeranstalterByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return loadVeranstalterByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter loadVeranstalterByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Veranstalter[] veranstalters = listVeranstalterByQuery(session, condition, orderBy);
		if (veranstalters != null && veranstalters.length > 0)
			return veranstalters[0];
		else
			return null;
	}
	
	public static Veranstalter loadVeranstalterByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Veranstalter[] veranstalters = listVeranstalterByQuery(session, condition, orderBy, lockMode);
		if (veranstalters != null && veranstalters.length > 0)
			return veranstalters[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateVeranstalterByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateVeranstalterByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateVeranstalterByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = EventmanagerPersistentManager.instance().getSession();
			return iterateVeranstalterByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateVeranstalterByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Veranstalter as Veranstalter");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateVeranstalterByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From event.model.Veranstalter as Veranstalter");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Veranstalter", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Veranstalter createVeranstalter() {
		return new event.model.Veranstalter();
	}
	
	public static boolean save(event.model.Veranstalter veranstalter) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().saveObject(veranstalter);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(event.model.Veranstalter veranstalter) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().deleteObject(veranstalter);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Veranstalter veranstalter)throws PersistentException {
		try {
			if (veranstalter.getAdmin() != null) {
				veranstalter.getAdmin().verantwortlicher.remove(veranstalter);
			}
			
			event.model.Event[] lEventss = veranstalter.events.toArray();
			for(int i = 0; i < lEventss.length; i++) {
				lEventss[i].setVerantwortlicher(null);
			}
			return delete(veranstalter);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(event.model.Veranstalter veranstalter, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (veranstalter.getAdmin() != null) {
				veranstalter.getAdmin().verantwortlicher.remove(veranstalter);
			}
			
			event.model.Event[] lEventss = veranstalter.events.toArray();
			for(int i = 0; i < lEventss.length; i++) {
				lEventss[i].setVerantwortlicher(null);
			}
			try {
				session.delete(veranstalter);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(event.model.Veranstalter veranstalter) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().refresh(veranstalter);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(event.model.Veranstalter veranstalter) throws PersistentException {
		try {
			EventmanagerPersistentManager.instance().getSession().evict(veranstalter);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
