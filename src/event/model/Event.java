/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package event.model;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Event")
public class Event implements Serializable {
	public Event() {
	}
	
	public boolean equals(Object aObj) {
		if (aObj == this)
			return true;
		if (!(aObj instanceof Event))
			return false;
		Event event = (Event)aObj;
		if ((getBezeichnung() != null && !getBezeichnung().equals(event.getBezeichnung())) || (getBezeichnung() == null && event.getBezeichnung() != null))
			return false;
		return true;
	}
	
	public int hashCode() {
		int hashcode = 0;
		hashcode = hashcode + (getBezeichnung() == null ? 0 : getBezeichnung().hashCode());
		return hashcode;
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_EVENT_ARTISTEN) {
			return ORM_artisten;
		}
		else if (key == ORMConstants.KEY_EVENT_TEILNEHMER) {
			return ORM_teilnehmer;
		}
		else if (key == ORMConstants.KEY_EVENT_ORT) {
			return ORM_ort;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_EVENT_VERANTWORTLICHER) {
			this.Verantwortlicher = (event.model.Veranstalter) owner;
		}
		
		else if (key == ORMConstants.KEY_EVENT_EVENTTHEMA) {
			this.EventThema = (event.model.Thema) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="Bezeichnung", nullable=false, length=255)	
	@Id	
	private String bezeichnung;
	
	@Column(name="Zeitpunkt", nullable=true)	
	private java.sql.Timestamp zeitpunkt;
	
	@ManyToOne(targetEntity=event.model.Veranstalter.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="VeranstalterID", referencedColumnName="ID", nullable=false) })	
	private event.model.Veranstalter Verantwortlicher;
	
	@ManyToOne(targetEntity=event.model.Thema.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="ThemaBez", referencedColumnName="Bez", nullable=false) })	
	private event.model.Thema EventThema;
	
	@ManyToMany(targetEntity=event.model.Darsteller.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinTable(name="Darsteller_Event", joinColumns={ @JoinColumn(name="EventBezeichnung") }, inverseJoinColumns={ @JoinColumn(name="DarstellerKünstlerName") })	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_artisten = new java.util.HashSet();
	
	@ManyToMany(targetEntity=event.model.Besucher.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinTable(name="Besucher_Event", joinColumns={ @JoinColumn(name="EventBezeichnung") }, inverseJoinColumns={ @JoinColumn(name="BesucherID") })	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_teilnehmer = new java.util.HashSet();
	
	@ManyToMany(mappedBy="ORM_events", targetEntity=event.model.EventOrt.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_ort = new java.util.HashSet();
	
	public void setBezeichnung(String value) {
		this.bezeichnung = value;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	public String getORMID() {
		return getBezeichnung();
	}
	
	public void setZeitpunkt(java.sql.Timestamp value) {
		this.zeitpunkt = value;
	}
	
	public java.sql.Timestamp getZeitpunkt() {
		return zeitpunkt;
	}
	
	private void setORM_Artisten(java.util.Set value) {
		this.ORM_artisten = value;
	}
	
	private java.util.Set getORM_Artisten() {
		return ORM_artisten;
	}
	
	@Transient	
	public final event.model.DarstellerSetCollection artisten = new event.model.DarstellerSetCollection(this, _ormAdapter, ORMConstants.KEY_EVENT_ARTISTEN, ORMConstants.KEY_DARSTELLER_AUFTRITTE, ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	private void setORM_Teilnehmer(java.util.Set value) {
		this.ORM_teilnehmer = value;
	}
	
	private java.util.Set getORM_Teilnehmer() {
		return ORM_teilnehmer;
	}
	
	@Transient	
	public final event.model.BesucherSetCollection teilnehmer = new event.model.BesucherSetCollection(this, _ormAdapter, ORMConstants.KEY_EVENT_TEILNEHMER, ORMConstants.KEY_BESUCHER_EVENTS, ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public void setVerantwortlicher(event.model.Veranstalter value) {
		if (Verantwortlicher != null) {
			Verantwortlicher.events.remove(this);
		}
		if (value != null) {
			value.events.add(this);
		}
	}
	
	public event.model.Veranstalter getVerantwortlicher() {
		return Verantwortlicher;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Verantwortlicher(event.model.Veranstalter value) {
		this.Verantwortlicher = value;
	}
	
	private event.model.Veranstalter getORM_Verantwortlicher() {
		return Verantwortlicher;
	}
	
	private void setORM_Ort(java.util.Set value) {
		this.ORM_ort = value;
	}
	
	private java.util.Set getORM_Ort() {
		return ORM_ort;
	}
	
	@Transient	
	public final event.model.EventOrtSetCollection ort = new event.model.EventOrtSetCollection(this, _ormAdapter, ORMConstants.KEY_EVENT_ORT, ORMConstants.KEY_EVENTORT_EVENTS, ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public void setEventThema(event.model.Thema value) {
		if (EventThema != null) {
			EventThema.events.remove(this);
		}
		if (value != null) {
			value.events.add(this);
		}
	}
	
	public event.model.Thema getEventThema() {
		return EventThema;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_EventThema(event.model.Thema value) {
		this.EventThema = value;
	}
	
	private event.model.Thema getORM_EventThema() {
		return EventThema;
	}
	
	public void anlegen() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public void löschen() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public void bearbeiten() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public void übertragen() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getBezeichnung());
	}
	
}
