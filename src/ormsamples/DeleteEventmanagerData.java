/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteEventmanagerData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = event.model.EventmanagerPersistentManager.instance().getSession().beginTransaction();
		try {
			event.model.Event leventmodelEvent = event.model.EventDAO.loadEventByQuery(null, null);
			// Delete the persistent object
			event.model.EventDAO.delete(leventmodelEvent);
			event.model.EventOrt leventmodelEventOrt = event.model.EventOrtDAO.loadEventOrtByQuery(null, null);
			// Delete the persistent object
			event.model.EventOrtDAO.delete(leventmodelEventOrt);
			event.model.Darsteller leventmodelDarsteller = event.model.DarstellerDAO.loadDarstellerByQuery(null, null);
			// Delete the persistent object
			event.model.DarstellerDAO.delete(leventmodelDarsteller);
			event.model.Besucher leventmodelBesucher = event.model.BesucherDAO.loadBesucherByQuery(null, null);
			// Delete the persistent object
			event.model.BesucherDAO.delete(leventmodelBesucher);
			event.model.Administrator leventmodelAdministrator = event.model.AdministratorDAO.loadAdministratorByQuery(null, null);
			// Delete the persistent object
			event.model.AdministratorDAO.delete(leventmodelAdministrator);
			event.model.Veranstalter leventmodelVeranstalter = event.model.VeranstalterDAO.loadVeranstalterByQuery(null, null);
			// Delete the persistent object
			event.model.VeranstalterDAO.delete(leventmodelVeranstalter);
			event.model.Thema leventmodelThema = event.model.ThemaDAO.loadThemaByQuery(null, null);
			// Delete the persistent object
			event.model.ThemaDAO.delete(leventmodelThema);
			event.model.Buehne leventmodelBuehne = event.model.BuehneDAO.loadBuehneByQuery(null, null);
			// Delete the persistent object
			event.model.BuehneDAO.delete(leventmodelBuehne);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteEventmanagerData deleteEventmanagerData = new DeleteEventmanagerData();
			try {
				deleteEventmanagerData.deleteTestData();
			}
			finally {
				event.model.EventmanagerPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
