/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateEventmanagerData {
	public void createTestData() throws PersistentException {
		// Insert sample data
		PersistentSession session = event.model.EventmanagerPersistentManager.instance().getSession();
		PersistentTransaction transaction = session.beginTransaction();
		try {
			session.doWork(new org.hibernate.jdbc.Work() {
				public void execute(java.sql.Connection conn) throws java.sql.SQLException {
					java.sql.Statement statement = conn.createStatement();
					statement.executeUpdate("INSERT INTO Administrator(ID, Username, Passwort, Vorname, Nachname) VALUES (125, 'mm', '123456', 'Max', 'Mustermann')");
					statement.executeUpdate("INSERT INTO Administrator(ID, Username, Passwort, Vorname, Nachname) VALUES (126, 'admin', 'admin', 'Donald', 'Trump')");
					statement.executeUpdate("INSERT INTO Buehne(Bez) VALUES ('Bühne')");
					statement.executeUpdate("INSERT INTO Buehne(Bez) VALUES ('Kirche')");
					statement.executeUpdate("INSERT INTO Buehne(Bez) VALUES ('Halle')");
					statement.executeUpdate("INSERT INTO Buehne(Bez) VALUES ('Ufer')");
					statement.executeUpdate("INSERT INTO Darsteller(KünstlerName) VALUES ('Big Shaq')");
					statement.executeUpdate("INSERT INTO Darsteller(KünstlerName) VALUES ('Moneyboy')");
					statement.executeUpdate("INSERT INTO Darsteller(KünstlerName) VALUES ('Die Toten Rosen')");
					statement.executeUpdate("INSERT INTO Darsteller(KünstlerName) VALUES ('Dirty Saints')");
					statement.executeUpdate("INSERT INTO Darsteller(KünstlerName) VALUES ('Stupido')");
					statement.executeUpdate("INSERT INTO Darsteller(KünstlerName) VALUES ('Weird Al Yankovic')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (1, 'Gerald', 'Permatier')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (275, 'Klara', 'Fall')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (276, 'Reiner', 'Zufall')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (277, 'Marie', 'Müller')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (278, 'Gernot', 'Hassknecht')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (279, 'Gerhart', 'Reinholzen')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (280, 'Waldraut', 'Doppler')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (281, 'Eddard', 'Stark')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (282, 'Ben', 'Solo')");
					statement.executeUpdate("INSERT INTO Besucher(ID, Vorname, Nachname) VALUES (283, 'Kunigunde', 'Schnäbele')");
					statement.executeUpdate("INSERT INTO Veranstalter(ID, Username, Passwort, Vorname, Nachname, AdministratorID) VALUES (175, 'Loewenzahn', 'password', 'Peter', 'Lustig', 125)");
					statement.executeUpdate("INSERT INTO Veranstalter(ID, Username, Passwort, Vorname, Nachname, AdministratorID) VALUES (571, 'zf', '123456', 'Rainer', 'Zufall', 125)");
					statement.executeUpdate("INSERT INTO Veranstalter(ID, Username, Passwort, Vorname, Nachname, AdministratorID) VALUES (572, 'Der Kraxxler', 'iliketrains', 'Micha', 'Heiss', 125)");
					statement.executeUpdate("INSERT INTO Thema(Bez) VALUES ('Musik')");
					statement.executeUpdate("INSERT INTO Thema(Bez) VALUES ('Tanz')");
					statement.executeUpdate("INSERT INTO Thema(Bez) VALUES ('Spiel')");
					statement.executeUpdate("INSERT INTO Thema(Bez) VALUES ('Kinder')");
					statement.executeUpdate("INSERT INTO Thema(Bez) VALUES ('Garten')");
					statement.executeUpdate("INSERT INTO Event(Bezeichnung, Zeitpunkt, VeranstalterID, ThemaBez) VALUES ('Die Große Buga Sause', '2019-05-05 20:00:00', 175, 'Musik')");
					statement.executeUpdate("INSERT INTO EventOrt(Name, Lat, Lon, Kapazität, BuehneBez) VALUES ('Hauptbühne', 49.1449, 9.20608, 1000, 'Bühne')");
					statement.executeUpdate("INSERT INTO EventOrt(Name, Lat, Lon, Kapazität, BuehneBez) VALUES ('Fährbühne', 49.1492, 9.20930, 500, 'Bühne')");
					statement.executeUpdate("INSERT INTO EventOrt(Name, Lat, Lon, Kapazität, BuehneBez) VALUES ('BuGaKirche', 49.1466, 9.20911, 100, 'Kirche')");
					statement.executeUpdate("INSERT INTO EventOrt(Name, Lat, Lon, Kapazität, BuehneBez) VALUES ('Blumenhalle', 49.1453, 9.20905, 250, 'Halle')");
					statement.executeUpdate("INSERT INTO EventOrt(Name, Lat, Lon, Kapazität, BuehneBez) VALUES ('Rollsporthalle', 49.1475, 9.21340, 250, 'Halle')");
					statement.executeUpdate("INSERT INTO Besucher_Event(BesucherID, EventBezeichnung) VALUES (1, 'Die Große Buga Sause')");
					statement.executeUpdate("INSERT INTO Besucher_Event(BesucherID, EventBezeichnung) VALUES (276, 'Die Große Buga Sause')");
					statement.executeUpdate("INSERT INTO Darsteller_Event(DarstellerKünstlerName, EventBezeichnung) VALUES ('Big Shaq', 'Die Große Buga Sause')");
					statement.executeUpdate("INSERT INTO Darsteller_Event(DarstellerKünstlerName, EventBezeichnung) VALUES ('Moneyboy', 'Die Große Buga Sause')");
					statement.executeUpdate("INSERT INTO Darsteller_Event(DarstellerKünstlerName, EventBezeichnung) VALUES ('Die Toten Rosen', 'Die Große Buga Sause')");
					statement.executeUpdate("INSERT INTO Darsteller_Event(DarstellerKünstlerName, EventBezeichnung) VALUES ('Weird Al Yankovic', 'Die Große Buga Sause')");
					statement.executeUpdate("INSERT INTO Event_EventOrt(EventBezeichnung, EventOrtName) VALUES ('Die Große Buga Sause', 'Hauptbühne')");
					statement.close();
				}
			});
			transaction.commit();
		}
		catch (Exception e) {
			try {
				transaction.rollback();
			}
			catch (PersistentException e1) {
				e.printStackTrace();
			}
			e.printStackTrace();
		}
		
		PersistentTransaction t = event.model.EventmanagerPersistentManager.instance().getSession().beginTransaction();
		try {
			event.model.Event leventmodelEvent = event.model.EventDAO.createEvent();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Ort, Teilnehmer, Artisten, EventThema, Verantwortlicher, bezeichnung
			event.model.EventDAO.save(leventmodelEvent);
			event.model.EventOrt leventmodelEventOrt = event.model.EventOrtDAO.createEventOrt();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Events, BuehnenArt, kapazität, lon, lat, name
			event.model.EventOrtDAO.save(leventmodelEventOrt);
			event.model.Darsteller leventmodelDarsteller = event.model.DarstellerDAO.createDarsteller();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Auftritte, künstlerName
			event.model.DarstellerDAO.save(leventmodelDarsteller);
			event.model.Besucher leventmodelBesucher = event.model.BesucherDAO.createBesucher();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Events
			event.model.BesucherDAO.save(leventmodelBesucher);
			event.model.Administrator leventmodelAdministrator = event.model.AdministratorDAO.createAdministrator();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Verantwortlicher, username
			event.model.AdministratorDAO.save(leventmodelAdministrator);
			event.model.Veranstalter leventmodelVeranstalter = event.model.VeranstalterDAO.createVeranstalter();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Events, Admin, username
			event.model.VeranstalterDAO.save(leventmodelVeranstalter);
			event.model.Thema leventmodelThema = event.model.ThemaDAO.createThema();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Events, bez
			event.model.ThemaDAO.save(leventmodelThema);
			event.model.Buehne leventmodelBuehne = event.model.BuehneDAO.createBuehne();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : Ort, bez
			event.model.BuehneDAO.save(leventmodelBuehne);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateEventmanagerData createEventmanagerData = new CreateEventmanagerData();
			try {
				createEventmanagerData.createTestData();
			}
			finally {
				event.model.EventmanagerPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
