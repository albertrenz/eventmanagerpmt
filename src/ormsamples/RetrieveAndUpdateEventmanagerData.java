/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateEventmanagerData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = event.model.EventmanagerPersistentManager.instance().getSession().beginTransaction();
		try {
			event.model.Event leventmodelEvent = event.model.EventDAO.loadEventByQuery(null, null);
			// Update the properties of the persistent object
			event.model.EventDAO.save(leventmodelEvent);
			event.model.EventOrt leventmodelEventOrt = event.model.EventOrtDAO.loadEventOrtByQuery(null, null);
			// Update the properties of the persistent object
			event.model.EventOrtDAO.save(leventmodelEventOrt);
			event.model.Darsteller leventmodelDarsteller = event.model.DarstellerDAO.loadDarstellerByQuery(null, null);
			// Update the properties of the persistent object
			event.model.DarstellerDAO.save(leventmodelDarsteller);
			event.model.Besucher leventmodelBesucher = event.model.BesucherDAO.loadBesucherByQuery(null, null);
			// Update the properties of the persistent object
			event.model.BesucherDAO.save(leventmodelBesucher);
			event.model.Administrator leventmodelAdministrator = event.model.AdministratorDAO.loadAdministratorByQuery(null, null);
			// Update the properties of the persistent object
			event.model.AdministratorDAO.save(leventmodelAdministrator);
			event.model.Veranstalter leventmodelVeranstalter = event.model.VeranstalterDAO.loadVeranstalterByQuery(null, null);
			// Update the properties of the persistent object
			event.model.VeranstalterDAO.save(leventmodelVeranstalter);
			event.model.Thema leventmodelThema = event.model.ThemaDAO.loadThemaByQuery(null, null);
			// Update the properties of the persistent object
			event.model.ThemaDAO.save(leventmodelThema);
			event.model.Buehne leventmodelBuehne = event.model.BuehneDAO.loadBuehneByQuery(null, null);
			// Update the properties of the persistent object
			event.model.BuehneDAO.save(leventmodelBuehne);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateEventmanagerData retrieveAndUpdateEventmanagerData = new RetrieveAndUpdateEventmanagerData();
			try {
				retrieveAndUpdateEventmanagerData.retrieveAndUpdateTestData();
			}
			finally {
				event.model.EventmanagerPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
