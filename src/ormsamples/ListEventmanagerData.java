/**
 * Licensee: Christian Moll(Hochschule Heilbronn)
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListEventmanagerData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Event...");
		event.model.Event[] eventmodelEvents = event.model.EventDAO.listEventByQuery(null, null);
		int length = Math.min(eventmodelEvents.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelEvents[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EventOrt...");
		event.model.EventOrt[] eventmodelEventOrts = event.model.EventOrtDAO.listEventOrtByQuery(null, null);
		length = Math.min(eventmodelEventOrts.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelEventOrts[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Darsteller...");
		event.model.Darsteller[] eventmodelDarstellers = event.model.DarstellerDAO.listDarstellerByQuery(null, null);
		length = Math.min(eventmodelDarstellers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelDarstellers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Besucher...");
		event.model.Besucher[] eventmodelBesuchers = event.model.BesucherDAO.listBesucherByQuery(null, null);
		length = Math.min(eventmodelBesuchers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelBesuchers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Administrator...");
		event.model.Administrator[] eventmodelAdministrators = event.model.AdministratorDAO.listAdministratorByQuery(null, null);
		length = Math.min(eventmodelAdministrators.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelAdministrators[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Veranstalter...");
		event.model.Veranstalter[] eventmodelVeranstalters = event.model.VeranstalterDAO.listVeranstalterByQuery(null, null);
		length = Math.min(eventmodelVeranstalters.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelVeranstalters[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Thema...");
		event.model.Thema[] eventmodelThemas = event.model.ThemaDAO.listThemaByQuery(null, null);
		length = Math.min(eventmodelThemas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelThemas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Buehne...");
		event.model.Buehne[] eventmodelBuehnes = event.model.BuehneDAO.listBuehneByQuery(null, null);
		length = Math.min(eventmodelBuehnes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventmodelBuehnes[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public static void main(String[] args) {
		try {
			ListEventmanagerData listEventmanagerData = new ListEventmanagerData();
			try {
				listEventmanagerData.listTestData();
			}
			finally {
				event.model.EventmanagerPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
