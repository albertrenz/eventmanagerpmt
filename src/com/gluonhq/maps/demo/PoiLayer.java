/*
 * Copyright (c) 2016, Gluon
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GLUON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.gluonhq.maps.demo;

import com.gluonhq.maps.MapLayer;
import com.gluonhq.maps.MapPoint;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;

/**
 * A layer that allows to visualise points of interest.
 */
public class PoiLayer extends MapLayer {


  private final ObservableList<Pair<MapPoint, Node>> points = FXCollections.observableArrayList();

  public PoiLayer() {
  }

  public void addPoint(MapPoint p, Node icon) {
    points.add(new Pair(p, icon));
    this.getChildren().add(icon);
    this.markDirty();
  }


  public void addPopupPoint(MapPoint p, Node icon, String title, String time) {
    final Pair<MapPoint, Node> pair = new Pair(p, icon);
    icon.setOnMouseClicked(e -> {
      e.consume();
      showPopup(pair, title, time);
    });
    points.add(pair);
    this.getChildren().add(icon);
    this.markDirty();
  }

  @Override
  protected void layoutLayer() {
    for (Pair<MapPoint, Node> candidate : points) {
      MapPoint point = candidate.getKey();
      Node icon = candidate.getValue();
      Point2D mapPoint = baseMap.getMapPoint(point.getLatitude(), point.getLongitude());
      icon.setVisible(true);
      icon.setTranslateX(mapPoint.getX());
      icon.setTranslateY(mapPoint.getY());
    }
  }

  private void showPopup(Pair<MapPoint, Node> pair, String title, String time) {
    Node icon = pair.getValue();
    MapPoint point = pair.getKey();

    final Stage primaryStage = (Stage) icon.getScene().getWindow();
    final Stage popupStage = new Stage();
    popupStage.initStyle(StageStyle.UNDECORATED);
    popupStage.initModality(Modality.APPLICATION_MODAL);
    popupStage.initOwner(primaryStage);

    VBox box = new VBox(5);
    box.setPadding(new Insets(5));
    box.getChildren().addAll(new Label(title),
        new Label("Uhrzeit " + time));
    Label close = new Label("X");
    close.setOnMouseClicked(a -> popupStage.close());

    StackPane.setAlignment(close, Pos.TOP_RIGHT);
    final StackPane stackPane = new StackPane(box, close);
    stackPane.setPadding(new Insets(5));
    stackPane.setStyle("-fx-background-color: white;");

    Scene scene = new Scene(stackPane, 150, 80);
    Bounds iconBounds = icon.localToScene(icon.getBoundsInLocal());
    popupStage.setX(primaryStage.getX() + primaryStage.getScene().getX() + iconBounds.getMaxX());
    popupStage.setY(primaryStage.getY() + primaryStage.getScene().getY() + iconBounds.getMaxY());
    popupStage.setScene(scene);
    popupStage.show();
  }

}
